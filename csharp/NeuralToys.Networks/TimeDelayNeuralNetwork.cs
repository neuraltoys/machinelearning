﻿namespace NeuralToys.Networks
{
    using Functions;

    public class TimeDelayNeuralNetwork : Network
    {
        /// <summary>
        /// Gets or sets the trainer currently assigned to this network
        /// </summary>
        public Learning.IFeedTrainer Trainer { get; set; }

        public TimeDelayNeuralNetwork(IActivator activator, int delay, int inputSize, params int[] layerSize)
            : base(activator, delay, inputSize, layerSize)
        {
        }
    }
}
