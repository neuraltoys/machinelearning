﻿namespace NeuralToys.Networks.Learning
{
    public interface IFeedTrainer
    {
        /// <summary>
        /// Train the neural network on a feed of data
        /// </summary>
        /// <param name="data">A continuous feed of data</param>
        void Train(double[] data);
    }
}
