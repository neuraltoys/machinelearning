﻿namespace NeuralToys.Networks.Learning
{
    using System;
    using System.Linq;

    /// <summary>
    /// Stochastic gradient BackPropagation training algorithm
    /// </summary>
    [Serializable]
    public class BackPropagationSetTrainer : BackPropagationTrainer, ISetTrainer
    {
        public BackPropagationSetTrainer(Network network)
            : base(network)
        {
        }

        /// <summary>
        /// Train the neural network on a set of examples
        /// </summary>
        /// <param name="inputs">A set of input vectors</param>
        /// <param name="outputs">A set of corresponding expected output vectors</param>
        public void Train(double[][] inputs, double[][] outputs)
        {
            this.Validate(inputs, outputs);
            var totalError = double.MaxValue;
            var epoch = 0;

            // initialize matrix of errors (delta)
            var deltaErrors = new double[this.Network.LayerCount][];
            for (var i = 0; i < this.Network.LayerCount; i++)
            {
                deltaErrors[i] = new double[this.Network[i].Size];
            }

            // continue until we've reached the epoch limit or the error is small enough
            while (epoch < this.MaxEpochs && totalError > this.ErrorTreshold)
            {
                totalError = 0;
                for (var i = 0; i < inputs.Length; i++)
                {
                    // run the example through the network
                    var result = this.Network.Forward(inputs[i]);
                    // calulate the output differences
                    var outputDiffs = result.Select((x, j) => outputs[i][j] - x).ToArray();
                    // the total error is the sum of squares
                    totalError += outputDiffs.Select(x => x * x).Sum() / 2.0;

                    // compute the errors for each neuron
                    for (var l = this.Network.LayerCount - 1; l >= 0; l--)
                    {
                        if (l == this.Network.LayerCount - 1)
                        {
                            // output layer
                            for (var n = 0; n < this.Network[l].Size; n++)
                            {
                                deltaErrors[l][n] = this.Network[l][n].PrimeOutput * outputDiffs[n];
                            }
                            continue;
                        }

                        // hidden layers
                        for (var n = 0; n < this.Network[l].Size; n++)
                        {
                            double sum = 0;
                            for (var k = 0; k < this.Network[l + 1].Size; k++)
                            {
                                for (var d = 0; d < this.Network[l + 1][k][n].Delay; d++)
                                    sum += deltaErrors[l + 1][k] * this.Network[l + 1][k][n][d].Current;
                            }
                            deltaErrors[l][n] = this.Network[l][n].PrimeOutput * sum;
                        }
                    }

                    // set the weights
                    for (var l = 0; l < this.Network.LayerCount; l++)
                    {
                        if (l == 0)
                        {
                            // update links for each neuron
                            for (var n = 0; n < this.Network[l].Size; n++)
                            {
                                // update weights for each link
                                for (var k = 0; k < this.Network[l][n].Links; k++)
                                {
                                    this.Network[l][n][k].Update(this.Network[l][n][k].WeightDiff.Select(
                                        x => this.LearningRate * (k == 0 ? -1 : inputs[i][k - 1]) * deltaErrors[l][n] + this.Momentum * x).Average());
                                }
                            }
                        }
                        else
                        {
                            // get the input of the previous layer
                            var layerInput = this.Network[l - 1].Output;

                            // update links for each neuron
                            for (var n = 0; n < this.Network[l].Size; n++)
                            {
                                // update weights for each link
                                for (var k = 0; k < this.Network[l][n].Links; k++)
                                {
                                    this.Network[l][n][k].Update(this.Network[l][n][k].WeightDiff.Select(
                                        (x, idx) => this.LearningRate * (k == 0 ? -1 : layerInput[k - 1][idx]) * deltaErrors[l][n] + this.Momentum * x).Average());
                                }
                            }
                        }
                    }
                }
                epoch++;
            }
        }

        /// <summary>
        /// Validate the input set of training examples
        /// </summary>
        /// <param name="inputs">A set of input arrays</param>
        /// <param name="outputs">A set of output arrays</param>
        protected void Validate(double[][] inputs, double[][] outputs)
        {
            if (inputs.Length < 1)
            {
                throw new ArgumentException("invalid input data");
            }

            if (outputs.Length < 1)
            {
                throw new ArgumentException("invalid output data");
            }

            if (inputs.Length != outputs.Length)
            {
                throw new ArgumentException("the output set does not match the input set");
            }
        }
    }
}
