﻿namespace NeuralToys.Networks.Learning
{
    public abstract class BackPropagationTrainer : Trainer
    {
        private double _learningRate = 0.2;
        private double _momentum = 0.2;

        /// <summary>
        /// The learning rate used for training
        /// </summary>
        public double LearningRate
        {
            get { return this._learningRate; }
            set { this._learningRate = (value > 0) ? value : this._learningRate; }
        }

        /// <summary>
        /// The momentum used to weight previous differences
        /// </summary>
        public double Momentum
        {
            get { return this._momentum; }
            set { this._momentum = (value > 0) ? value : this._momentum; }
        }

        protected BackPropagationTrainer(Network n)
            : base(n)
        {
        }
    }
}
