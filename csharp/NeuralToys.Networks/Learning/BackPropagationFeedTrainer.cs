﻿namespace NeuralToys.Networks.Learning
{
    using System;
    using System.Linq;

    /// <summary>
    /// Stochastic gradient BackPropagation training algorithm
    /// </summary>
    [Serializable]
    public class BackPropagationFeedTrainer : BackPropagationTrainer, IFeedTrainer
    {
        public BackPropagationFeedTrainer(Network network)
            : base(network)
        {
        }

        /// <summary>
        /// Train the neural network on a continuous data feed
        /// </summary>
        /// <param name="data">The data feed</param>
        public void Train(double[] data)
        {
            this.Validate(data);
            var totalError = double.MaxValue;
            var epoch = 0;

            // initialize matrix of errors (delta)
            var deltaErrors = new double[this.Network.LayerCount][];
            for (var i = 0; i < this.Network.LayerCount; i++)
            {
                deltaErrors[i] = new double[this.Network[i].Size];
            }

            // continue until we've reached the epoch limit or the error is small enough
            while (epoch < this.MaxEpochs && totalError > this.ErrorTreshold)
            {
                var start = 0;
                totalError = 0;
                while (start + this.Network.InputSize + this.Network.OutputSize < data.Length)
                {
                    var inputs = data.Skip(start).Take(this.Network.InputSize).ToArray();
                    var outputs = data.Skip(start + this.Network.InputSize).Take(this.Network.OutputSize).ToArray();
                    // run the example through the network
                    var result = this.Network.Forward(inputs);
                    // calulate the output differences
                    var outputDiffs = result.Select((x, j) => outputs[j] - x).ToArray();
                    // the total error is the sum of squares
                    totalError += outputDiffs.Select(x => x * x).Sum() / 2.0;

                    // compute the errors for each neuron
                    for (var l = this.Network.LayerCount - 1; l >= 0; l--)
                    {
                        if (l == this.Network.LayerCount - 1)
                        {
                            // output layer
                            for (var n = 0; n < this.Network[l].Size; n++)
                            {
                                deltaErrors[l][n] = this.Network[l][n].PrimeOutput * outputDiffs[n];
                            }
                            continue;
                        }

                        // hidden layers
                        for (var n = 0; n < this.Network[l].Size; n++)
                        {
                            double sum = 0;
                            for (var k = 0; k < this.Network[l + 1].Size; k++)
                            {
                                for (var d = 0; d < this.Network[l + 1][k][n].Delay; d++)
                                    sum += deltaErrors[l + 1][k] * this.Network[l + 1][k][n][d].Current;
                            }
                            deltaErrors[l][n] = this.Network[l][n].PrimeOutput * sum;
                        }
                    }

                    // set the weights
                    for (var l = 0; l < this.Network.LayerCount; l++)
                    {
                        if (l == 0)
                        {
                            // update links for each neuron
                            for (var n = 0; n < this.Network[l].Size; n++)
                            {
                                // update weights for each link
                                for (var k = 0; k < this.Network[l][n].Links; k++)
                                {
                                    this.Network[l][n][k].Update(this.Network[l][n][k].WeightDiff.Select(
                                        x => this.LearningRate * (k == 0 ? -1 : inputs[k - 1]) * deltaErrors[l][n] + this.Momentum * x).Average());
                                }
                            }
                        }
                        else
                        {
                            // get the input of the previous layer
                            var layerInput = this.Network[l - 1].Output;

                            // update links for each neuron
                            for (var n = 0; n < this.Network[l].Size; n++)
                            {
                                // update weights for each link
                                for (var k = 0; k < this.Network[l][n].Links; k++)
                                {
                                    this.Network[l][n][k].Update(this.Network[l][n][k].WeightDiff.Select(
                                        (x, i) => this.LearningRate * (k == 0 ? -1 : layerInput[k - 1][i]) * deltaErrors[l][n] + this.Momentum * x).Average());
                                }
                            }
                        }
                    }
                    start++;
                }
                Console.WriteLine(totalError);
                epoch++;
            }
        }

        /// <summary>
        /// Validate the input data and network
        /// </summary>
        /// <param name="data">The input feed</param>
        protected void Validate(double[] data)
        {
            if (data.Length < this.Network.InputSize + this.Network[this.Network.LayerCount - 1].Size)
            {
                throw new ArgumentException("invalid input data");
            }
        }
    }
}
