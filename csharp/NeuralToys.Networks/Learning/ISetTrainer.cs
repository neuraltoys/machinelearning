﻿namespace NeuralToys.Networks.Learning
{
    public interface ISetTrainer
    {
        /// <summary>
        /// Train the neural network on a set of examples
        /// </summary>
        /// <param name="inputs">A set of input vectors</param>
        /// <param name="outputs">A set of corresponding expected output vectors</param>
        void Train(double[][] inputs, double[][] outputs);
    }
}
