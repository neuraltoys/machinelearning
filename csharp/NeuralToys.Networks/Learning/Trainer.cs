﻿namespace NeuralToys.Networks.Learning
{
    using System;

    /// <summary>
    /// Base class for all learning algorithms
    /// </summary>
    [Serializable]
    public abstract class Trainer
    {
        private readonly Network _network;
        private double _errorThreshold = 0.001;
        private int _maxEpochs = 10000;

        protected Trainer(Network n)
        {
            this._network = n;
        }

        /// <summary>
        /// Get the neural network this learning algorithm is currently training
        /// </summary>
        protected Network Network
        {
            get { return this._network; }
        }

        /// <summary>
        /// Get or set the maximum sum of square errors value ( >0)
        /// </summary>
        public double ErrorTreshold
        {
            get { return this._errorThreshold; }
            set { this._errorThreshold = (value > 0) ? value : this._errorThreshold; }
        }
        /// <summary>
        /// Get or set the maximum number of learning iterations.
        /// </summary>
        public int MaxEpochs
        {
            get { return this._maxEpochs; }
            set { this._maxEpochs = (value > 0) ? value : this._maxEpochs; }
        }
    }
}
