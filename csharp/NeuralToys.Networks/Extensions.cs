﻿namespace NeuralToys.Networks
{
    using System;
    using System.Collections.Generic;

    internal static class Extensions
    {
        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (var item in enumeration)
            {
                action(item);
            }
        }
    }
}
