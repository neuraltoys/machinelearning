﻿namespace NeuralToys.Networks.Functions
{
    using System;

    /// <summary>
    /// The sigmoid activator
    /// </summary>
    [Serializable]
    public class SigmoidActivator : IActivator
    {
        /// <summary>
        /// Compute sigmoid for a given value
        /// </summary>
        public double Output(double x)
        {
            return 1 / (1 + Math.Exp(-x));
        }

        /// <summary>
        /// Compute the differential of sigmoid for a given value
        /// </summary>
        public double OutputPrime(double x)
        {
            double y = this.Output(x);
            return y * (1 - y);
        }
    }
}
