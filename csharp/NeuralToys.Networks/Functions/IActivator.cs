﻿namespace NeuralToys.Networks.Functions
{
    /// <summary>
    /// Interface of an activation function
    /// </summary>
    public interface IActivator
    {
        /// <summary>
        /// Compute function for a given value
        /// </summary>
        double Output(double x);

        /// <summary>
        /// Compute the differential of the function for a given value
        /// </summary>
        double OutputPrime(double x);
    }
}
