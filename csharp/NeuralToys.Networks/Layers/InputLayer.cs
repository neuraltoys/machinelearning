﻿namespace NeuralToys.Networks.Layers
{
    using System;
    using System.Linq;
    using Nodes;

    [Serializable]
    class InputLayer : ILayer
    {
        private readonly InputNode[] _nodes;

        public InputLayer(int inputs, int delay)
        {
            this._nodes = new InputNode[inputs];

            for (var i = 0; i < inputs; i++)
            {
                this._nodes[i] = new InputNode(delay);
            }
        }

        public InputNode this[int index]
        {
            get { return this._nodes[index]; }
        }

        public int Size
        {
            get { return this._nodes.Length; }
        }

        public double[][] Output
        {
            get { return this._nodes.Select(x => x.Output).ToArray(); }
        }

        public INode[] Nodes
        {
            get { return this._nodes.Select(x => (INode)x).ToArray(); }
        }

        public void SetInput(params double[] inputs)
        {
            for (var i = 0; i < inputs.Length; i++)
            {
                this._nodes[i].Set(inputs[i]);
            }
        }
    }
}
