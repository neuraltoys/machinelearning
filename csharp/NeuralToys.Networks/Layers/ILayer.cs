﻿namespace NeuralToys.Networks.Layers
{
    using Nodes;

    interface ILayer
    {
        int Size { get; }
        double[][] Output { get; }
        INode[] Nodes { get; }
    }
}
