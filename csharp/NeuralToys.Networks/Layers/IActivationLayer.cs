﻿namespace NeuralToys.Networks.Layers
{
    using Nodes;

    interface IActivationLayer : ILayer
    {
        IActivatorNode this[int index] { get; }
        void Compute();
    }
}
