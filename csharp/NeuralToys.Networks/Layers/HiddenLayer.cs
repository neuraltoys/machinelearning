﻿namespace NeuralToys.Networks.Layers
{
    using System;
    using System.Linq;
    using Functions;
    using Links;
    using Nodes;

    [Serializable]
    class HiddenLayer : IActivationLayer
    {
        private readonly IActivatorNode[] _nodes;

        public HiddenLayer(int nodes, INode[] inputs, IActivator activator, int delay)
        {
            this._nodes = new IActivatorNode[nodes];
            for (var i = 0; i < nodes; i++)
            {
                this._nodes[i] = new ActivatorNode(inputs.Select(x => (ILink)new Link(x)).ToArray(), activator, delay);
            }
        }

        public IActivatorNode this[int index]
        {
            get { return this._nodes[index]; }
        }

        public int Size
        {
            get { return this._nodes.Length; }
        }

        public double[][] Output
        {
            get { return this._nodes.Select(x => x.Output).ToArray(); }
        }

        public INode[] Nodes
        {
            get { return this._nodes.Select(x => (INode) x).ToArray(); }
        }

        public void Compute()
        {
            foreach (var node in this._nodes)
            {
                node.Compute();
            }
        }
    }
}
