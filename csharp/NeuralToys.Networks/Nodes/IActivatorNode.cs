﻿namespace NeuralToys.Networks.Nodes
{
    using Links;

    interface IActivatorNode : INode
    {
        int Links { get; }
        ILink this[int index] { get; }
        double PrimeOutput { get; }
        void Compute();
    }
}
