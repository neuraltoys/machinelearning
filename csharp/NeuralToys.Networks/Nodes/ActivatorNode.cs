﻿namespace NeuralToys.Networks.Nodes
{
    using System;
    using System.Linq;
    using Functions;
    using Links;

    [Serializable]
    class ActivatorNode : InputNode, IActivatorNode
    {
        private readonly ILink[] _inputLinks;
        private readonly IActivator _activator;
        private readonly double[] _preOutputs;

        public ActivatorNode(ILink[] inputLinks, IActivator activator, int delay)
            : base(delay)
        {
            this._inputLinks = new ILink[inputLinks.Length + 1];
            // put the threshold link at position 0
            this._inputLinks[0] = new Link();
            // copy the other links
            inputLinks.CopyTo(this._inputLinks, 1);
            this._activator = activator;
            this._preOutputs = new double[delay];
        }

        public ILink this[int index]
        {
            get { return this._inputLinks[index]; }
        }

        public int Links
        {
            get { return this._inputLinks.Length; }
        }

        protected double PreOutput
        {
            get { return this._preOutputs.Length == 1 ? this._preOutputs[0] : this._preOutputs.Take(this.Current).Average(); }
        }

        public double PrimeOutput
        {
            get { return this._activator != null ? this._activator.OutputPrime(this.PreOutput) : this.PreOutput; }
        }

        public void Compute()
        {
            for (var i = this._preOutputs.Length - 1; i > 0; i--)
            {
                this._preOutputs[i] = this._preOutputs[i - 1];
            }
            this._preOutputs[0] = this._inputLinks.Select(x => x.Output).Sum();
            this.Set(this._activator != null ? this._activator.Output(this._preOutputs[0]) : this._preOutputs[0]);
        }
    }
}
