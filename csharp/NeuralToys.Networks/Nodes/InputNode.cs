﻿namespace NeuralToys.Networks.Nodes
{
    using System;
    using System.Linq;

    [Serializable]
    class InputNode : INode
    {
        private readonly double[] _outputs;

        public InputNode(int delay)
        {
            this._outputs = new double[delay];
        }

        internal int Current { get; private set; }

        public virtual double[] Output
        {
            get
            {
                return this._outputs;
            }
        }

        public void Set(double value)
        {
            for (var i = this._outputs.Length - 1; i > 0; i--)
            {
                this._outputs[i] = this._outputs[i - 1];
            }
            this._outputs[0] = value;

            if (this.Current < this._outputs.Length)
            {
                this.Current++;
            }
        }
    }
}
