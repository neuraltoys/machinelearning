﻿namespace NeuralToys.Networks
{
    using Functions;

    public class MultiLayerPerceptron : Network
    {
        /// <summary>
        /// Gets or sets the trainer currently assigned to this network
        /// </summary>
        public Learning.ISetTrainer Trainer { get; set; }

        public MultiLayerPerceptron(IActivator activator, int inputSize, params int[] layerSize)
            : base(activator, 1, inputSize, layerSize)
        {
        }
    }
}
