namespace NeuralToys.Networks
{
    using System;
    using System.Linq;
    using Functions;
    using Layers;

    /// <summary>
    /// Represents an Artificial Neural Network
    /// </summary>  
    [Serializable]
    public abstract class Network
    {
        private readonly InputLayer _inputLayer;
        private readonly IActivationLayer[] _layers;

        public int InputSize
        {
            get { return this._inputLayer.Size; }
        }

        internal IActivationLayer this[int index]
        {
            get { return this._layers[index]; }
        }

        public int OutputSize
        {
            get { return this._layers[this.LayerCount - 1].Size; }
        }

        /// <summary>
        /// Number of layers in the network
        /// </summary>
        internal int LayerCount
        {
            get { return this._layers.Length; }
        }

        /// <summary>
        /// Create a new neural network
        /// </summary>
        /// <param name="activator">The activator function</param>
        /// <param name="delay">The time-delay of the network</param>
        /// <param name="inputSize">The number of input nodes in the network</param>
        /// <param name="layerSize">Number of neurons for each layer</param>
        protected Network(IActivator activator, int delay, int inputSize, params int[] layerSize)
        {
            if (layerSize.Length < 2)
                throw new ArgumentException("network must have at least an input layer and an output layer");

            this._layers = new IActivationLayer[layerSize.Length];
            this._inputLayer = new InputLayer(inputSize, delay);
            for (var i = 0; i < layerSize.Length; i++)
                this._layers[i] = new HiddenLayer(layerSize[i], i == 0 ? this._inputLayer.Nodes : this._layers[i-1].Nodes, activator, delay);
        }

        /// <summary>
        /// Compute the value for the specified input
        /// </summary>
        /// <param name="input">the input vector</param>
        /// <returns>the output vector of the neuronal network</returns>
        public double[] Forward(params double[] input)
        {
            if (input.Length != this._inputLayer.Size)
            {
                throw new ArgumentException("invalid input vector");
            }

            this._inputLayer.SetInput(input);
            for (var i = 0; i < this._layers.Length; i++) this._layers[i].Compute();
            return this._layers[this._layers.Length - 1].Output.Select(x => x[0]).ToArray();
        }
    }
}
