﻿namespace NeuralToys.Networks.Links
{
    interface ILink
    {
        double Output { get; }
        IWeight this[int index] { get; }
        double Delay { get; }
        double[] Weights { get; }
        double[] WeightDiff { get; }
        void Update(double difference);
    }
}
