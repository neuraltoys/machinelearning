﻿namespace NeuralToys.Networks.Links
{
    public class Weight : IWeight
    {
        private const double RAND_START = -1;
        private const double RAND_END = 1;

        private double _current;

        public double Current
        {
            get { return this._current; }
            set
            {
                this.Previous = this._current;
                this._current = value;
            }
        }

        public double Previous { get; private set; }

        public Weight()
        {
            this.Current = RAND_START + Utils.Rand.NextDouble() * (RAND_END - RAND_START);
        }

        public double Difference
        {
            get { return this._current - this.Previous; }
        }
    }
}
