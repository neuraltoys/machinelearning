﻿namespace NeuralToys.Networks.Links
{
    public interface IWeight
    {
        double Current { get; set; }
        double Difference { get; }
        double Previous { get; }
    }
}
