﻿namespace NeuralToys.Networks.Links
{
    using System.Linq;
    using Nodes;

    class Link : ILink
    {
        private readonly IWeight[] _weights;
        private readonly INode _inputNode;

        public Link() : this(null)
        {
        }

        public Link(INode inputNode)
        {
            this._inputNode = inputNode;
            this._weights = new IWeight[inputNode == null ? 1 : inputNode.Output.Length];
            for (var i = 0; i < this._weights.Length; i++)
            {
                this._weights[i] = new Weight();
            }
        }

        public IWeight this[int index]
        {
            get { return this._weights[index]; }
        }

        public double Delay
        {
            get { return this._weights.Length; }
        }

        public void Update(double difference)
        {
            foreach (var w in this._weights)
            {
                w.Current += difference;
            }
        }

        public double[] WeightDiff
        {
            get { return this._weights.Select(x => x.Difference).ToArray(); }
        }

        public double[] Weights
        {
            get { return this._weights.Select(x => x.Current).ToArray(); }
        }

        public double Output
        {
            get
            {
                if (this._inputNode == null) return -this._weights[0].Current;

                return this._weights.Select((w, i) => w.Current*this._inputNode.Output[i]).Sum();
            }
        }
    }
}
