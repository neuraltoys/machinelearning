﻿namespace UnitTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NeuralToys.Networks;
    using NeuralToys.Networks.Functions;
    using NeuralToys.Networks.Learning;

    [TestClass]
    public class Networks
    {
        [TestMethod]
        public void BooleanAnd()
        {
            var n = new MultiLayerPerceptron(new SigmoidActivator(), 2, 3, 1);
            n.Trainer = new BackPropagationSetTrainer(n);
            n.Trainer.Train(new[]
            {
                new double[] { 0, 0 },
                new double[] { 0, 1 },
                new double[] { 1, 0 },
                new double[] { 1, 1 }
            }, new[]
            {
                new double[] { 0 },
                new double[] { 0 },
                new double[] { 0 },
                new double[] { 1 }
            });
            Assert.IsTrue(n.Forward(0, 1)[0] < 0.1);
            Assert.IsTrue(n.Forward(1, 1)[0] > 0.9);
        }

        [TestMethod]
        public void BooleanXor()
        {
            var n = new MultiLayerPerceptron(new SigmoidActivator(), 2, 3, 1);
            n.Trainer = new BackPropagationSetTrainer(n);
            n.Trainer.Train(new[]
            {
                new double[] { 0, 0 },
                new double[] { 0, 1 },
                new double[] { 1, 0 },
                new double[] { 1, 1 }
            }, new[]
            {
                new double[] { 0 },
                new double[] { 1 },
                new double[] { 1 },
                new double[] { 0 }
            });
            Assert.IsTrue(n.Forward(0, 1)[0] > 0.9);
            Assert.IsTrue(n.Forward(1, 1)[0] < 0.1);
        }
    }
}
