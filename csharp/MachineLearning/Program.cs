﻿namespace MachineLearning
{
    using System;
    using NeuralToys.Networks;
    using NeuralToys.Networks.Functions;
    using NeuralToys.Networks.Learning;

    class Program
    {
        static void Main(string[] args)
        {
            const int ENTRIES = 100;
            var sequence = new double[100];
            for (var i = 0; i < ENTRIES; i++)
                sequence[i] = (i + 1) / 100.0;

            var net = new TimeDelayNeuralNetwork(new SigmoidActivator(), 10, 5, 5, 1);
            net.Trainer = new BackPropagationFeedTrainer(net);
            net.Trainer.Train(sequence);

            Console.WriteLine("77,78,79,80,81 = {0}", net.Forward(0.77, 0.78, 0.79, 0.80, 0.81)[0]);
            Console.WriteLine("21,22,23,24,25 = {0}", net.Forward(0.21, 0.22, 0.23, 0.24, 0.25)[0]);
            
            Console.ReadKey();
        }
    }
}
