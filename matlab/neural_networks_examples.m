%% Neural Networks
% Some examples of applications using Multilayered Neural Networks

%% AND operator
% A network emulation the AND boolean operator
training = [0 0 0; 0 1 0; 1 0 0; 1 1 1];
learningRate = 0.2;
maxError = 0.001;
momentum = 0.2;
network = Network(2, 3, 1);
network.Backpropagation(training, learningRate, maxError, momentum);
network.Forward([0;1])
network.Forward([1;1])