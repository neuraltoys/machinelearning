classdef Network < handle
    %NETWORK Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        inputSize
        hiddenSize
        outputSize
        
        W1 % the weights between input and hidden
        W1p % the previous weights between input and hidden
        W2 % the weights between hidden and output
        W2p % the previous weights between hidden and output
        H1 % input to the hidden layer
        H2 % output of the hidden layer
        O1 % input to the output layer
    end
    
    methods
        function obj = Network(inputSize, hiddenSize, outputSize)
            obj.inputSize = inputSize;
            obj.hiddenSize = hiddenSize;
            obj.outputSize = outputSize;
            
            % initialize all weights to random values in [-0.5, 0.5]
            obj.W1 = rand(obj.hiddenSize, obj.inputSize) - 0.5;
            obj.W2 = rand(obj.outputSize, obj.hiddenSize) - 0.5;
        end
        
        function Backpropagation(obj, trainingSet, learningRate, maxError, momentum)
            if nargin < 5
                momentum = 0;
                if nargin < 4
                    maxError = 0.1;
                    if nargin < 3
                        learningRate = 0.1;
                    end
                end
            end
            
            error = 1/0;
            trainingSets = size(trainingSet, 1);
            trainingInputs = trainingSet(:, 1:obj.inputSize);
            trainingOutputs = trainingSet(:, obj.inputSize + 1:obj.inputSize + obj.outputSize);
            
            if (momentum > 0)
                obj.W1p = obj.W1;
                obj.W2p = obj.W2;
            end

            % until termination condition is met do
            epoch = 1;
            while error > maxError && epoch < 10000

                error = 0;
                for t = 1:trainingSets
                    tInput = trainingInputs(t,:)';
                    tOutput = trainingOutputs(t,:)';
                    
                    % propagate the input forward through the network
                    outputs = obj.Forward(tInput);
                    outputDiff = tOutput - outputs;
                    % calculate total error
                    error = error + sum(sum(outputDiff.^2));

                    % calculate error for each output unit
                    deltaOutput = sigmoidPrime(obj.W2 * obj.H2) .* outputDiff;

                    % calculate error for each hidden unit
                    deltaHidden = sigmoidPrime(obj.W1 * tInput) .* (deltaOutput * obj.W2)';

                    % update weights between input and hidden
                    DeltaW1 = learningRate * (deltaHidden * tInput');
                    if (momentum > 0)
                        DeltaW1 = DeltaW1 + momentum * (obj.W1 - obj.W1p);
                        obj.W1p = obj.W1;
                    end
                    obj.W1 = obj.W1 + DeltaW1;
                    
                    % update weights between hidden and output
                    DeltaW2 = learningRate * (deltaOutput * sigmoid(obj.W1 * tInput))';
                    if (momentum > 0)
                        DeltaW2 = DeltaW2 + momentum * (obj.W2 - obj.W2p);
                        obj.W2p = obj.W2;
                    end
                    obj.W2 = obj.W2 + DeltaW2;

%                     outputOutput = sigmoid(obj.W2 * trainingHidden');
%                     error = sqrt(sum((tOutput - outputOutput).^2));
                end

                epoch = epoch+1;
            end
        end
        
        function outputs = Forward(obj, inputs)
            obj.H1 = obj.W1 * inputs;
            obj.H2 = sigmoid(obj.H1);
            obj.O1 = obj.W2 * obj.H2;
            outputs = sigmoid(obj.O1);
        end
    end
    
end

